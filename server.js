const path = require('path')
const express = require('express')
const router = express.Router();
const multer = require('multer')
const nodemailer = require("nodemailer");

const app = express()
const port = 3000;

router.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '/index.html'));
});

router.get('/catalog', (req, res) => {
    res.sendFile(path.join(__dirname, '/catalog.html'));
});

const transporter = nodemailer.createTransport({
    pool: true,
    host: "smtp.mail.ru",
    port: 465,
    secure: true,
    auth: {
        user: "zakaz@pr-mebel.ru",
        pass: "nobiele000",
    },
});

router.post('/send-email', multer({ storage: multer.memoryStorage() }).array("file[]"), async (req, res) => {
    const { email, phone, message, name, ...rest } = req.body;

    try {
        await transporter.sendMail({
            from: 'zakaz@pr-mebel.ru',
            to: 'zakaz@pr-mebel.ru',
            replyTo: email || 'zakaz@pr-mebel.ru',
            subject: `Расчет | ${name} | ${phone}`,
            html: `
                <p><strong>Имя:</strong><br>${name}</p>
                <p><strong>Телефон:</strong><br>${phone}</p>
                ${email ? `<p><strong>Почта:</strong><br>${email}</p>` : ''}
                ${message
                    ? `<p><strong>Описание:</strong><br>${message}</p>`
                    : ''
                }
                ${rest ? `<p>${JSON.stringify(rest, null, 4)}</p>` : ''}
            `,
            attachments: req.files.map((file) => ({
                filename: file.filename,
                content: file.buffer,
                contentType: file.mimetype,
            })),
        })

        res.sendStatus(200);
    } catch (e) {
        res.sendStatus(500).json(e);
    }
});

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', router);

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});

function a() {
    if (!grouped[variable.type]) {
        return {
            ...grouped,
            [variable.type]: {
                type: variable.type,
                data: [variable],
            },
        };
    }
    
    grouped[variable.type].data.push(variable);
    
    return grouped;
}