import { mobile } from './common';
import { incrementAmount } from './constants';

import $ from 'jquery';

function handleLoadMoreClick() {
  currentLimit += incrementAmount;
  mixer.paginate({ limit: currentLimit });

  setTimeout(function () {
    $(".catalog__actions").hide(0);
  }, 150);
}

console.log(mobile);

document.body.onload = function () {

  /**
   * Выключает глобальный лоадер
   */
  $('#cube-loader').addClass('done');
  /**
   * Показывает контент
   */
  $('body').removeClass('overflow-hidden');
}

$(document).ready(function () {

  setTimeout(function () {
    var elem = document.createElement('script');
    elem.type = 'text/javascript';
    elem.src = "//api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Abc51f7eaa7cfe8b1f22e3591d05a24cfd103f07cdd1b5c35353c2ac3a6b6826a&amp;width=100%25&amp;&amp;lang=ru_RU&amp;scroll=false";

    document.getElementById('map').appendChild(elem);

    (function (m, e, t, r, i, k, a) {
      m[i] = m[i] || function () {
        (m[i].a = m[i].a || []).push(arguments)
      };
      m[i].l = 1 * new Date();
      k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
      (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    $("body").addClass("ymConn");

    !function (f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function () {
        n.callMethod ?
          n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
      'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '364548827793450');
    fbq('track', 'PageView');


    (function (w, d, u) {
      var s = d.createElement('script');
      s.async = true;
      s.src = u + '?' + (Date.now() / 60000 | 0);
      var h = d.getElementsByTagName('script')[0];
      h.parentNode.insertBefore(s, h);
    })(window, document, 'https://cdn.bitrix24.ru/b2399491/crm/site_button/loader_3_q6v4tx.js');


    var _tmr = window._tmr || (window._tmr = []);
    _tmr.push({ id: "3136628", type: "pageView", start: (new Date()).getTime(), pid: "USER_ID" });
    (function (d, w, id) {
      if (d.getElementById(id)) return;
      var ts = d.createElement("script");
      ts.type = "text/javascript";
      ts.async = true;
      ts.id = id;
      ts.src = "https://top-fwz1.mail.ru/js/code.js";
      var f = function () {
        var s = d.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(ts, s);
      };
      if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
      } else {
        f();
      }
    })(document, window, "topmailru-code");


  }, 5000);
});