import './index.css'
import '../fonts/stylesheet.css';
import '../css/jquery.fancybox.min.css'
import '../css/main.min.css'

import $ from 'jquery';

let mobile;

/**
 * Выставляет высоту для главного слайдера
 */
export function heights() {
  $(".top__slide").css("height", $(window).height());
}

document.body.onload = function () {
  /**
   * Проверяет текущую ширину экрана
   */
  if ($(window).width() < 768) {
    mobile = "-mobile";
  } else {
    mobile = '-desktop';
  }

  /**
   * Выключает глобальный лоадер
   */
  $('#cube-loader').addClass('done');
  /**
   * Показывает контент
   */
  $('body').removeClass('overflow-hidden');

  // TODO: Что за heights?
  heights();
}



export { mobile };