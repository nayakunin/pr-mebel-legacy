import './index.css'
import '../fonts/stylesheet.css';
import '../css/jquery.fancybox.min.css'
import '../css/main.min.css'

import $ from 'jquery';
import 'slick-carousel';
import 'jquery-mask-plugin';

import { mobile, heights } from './common';

var mixer;
var sliderImage, sliderDescr;
var loadMoreEl;
var currentLimit = 30;
var incrementAmount = 200;
var contactVisible = false;

$.ajaxSetup({
  cache: true
});

document.body.onload = function () {
  /**
   * Прокручивает страницу
   */
  // TODO: Понять куда это прокручивает страницу
  if (window.location.hash != '') {
    window.hashName = window.location.hash;
    window.location.hash = '';
    $('html').animate({ scrollTop: $(window.hashName).offset().top - 50 }, 50, function () {
      window.location.hash = window.hashName;
    });
  }

  /**
   * Выключает глобальный лоадер
   */
  $('#cube-loader').addClass('done');
  /**
   * Показывает контент
   */
  $('body').removeClass('overflow-hidden');
}

$(document).ready(function () {
  if ($("*").is(".catalog__container")) {
    var $element = $('.catalog__container');
    $(window).scroll(function () {
      var scroll = $(window).scrollTop() + $(window).height() - 150;
      var offset = $element.offset().top

      if ((scroll > offset) && (scroll < $element.height() + offset + 150)) {
        $('.controls').addClass("open");
      } else {
        $('.controls').removeClass("open");
      }
    });
  }

  $("body").on("click", ".down-link, .main-link, .scroll", function (event) {
    elementClick = $(this).attr("href");
    destination = $(elementClick).offset().top - 50;
    $("body,html").animate({ scrollTop: destination }, 250);
    return false;
  });

  if ($(window).width() < 1200) {
    $('.section').addClass('fp-auto-height-responsive');
  }

  $('.top__slider').slick({
    autoplay: true,
    autoplaySpeed: 6000,
    speed: 200,
    dots: true,
    infinite: true,
    touchThreshold: 50,
    useCSS: true,
    useTransform: true,
    appendDots: '.top__dots .container',
    responsive: [
      {
        breakpoint: 992,
        settings: {
          speed: 100,
          touchThreshold: 35,
          useCSS: false,
          useTransform: false
        }
      }
    ]
  });

  $('.top__dots button').each(function () {
    if ($(this).text().length < 2) {
      $(this).text("0" + $(this).text());
    }
  });

  $(".catalog__container").on('click', '.wrapper', function () {
    $(this).parents(".catalog__item").find(".collection-name").trigger('click');
  });

  $(".catalog__container").on('click', '.collection-name', function () {
    var slideNumber = $(".catalog__container .catalog__item:visible").index($(this).parents(".catalog__item"));

    var filteredSlick = "";
    $(".catalog__container .mixitup-control-active").each(function () {
      if ($(this).data("filter") != "") {
        filteredSlick = filteredSlick + $(this).data("filter");
      }
    });

    sliderImage.slick('slickUnfilter');
    sliderDescr.slick('slickUnfilter');

    if (filteredSlick != "") {
      sliderImage.slick('slickFilter', filteredSlick);
      sliderDescr.slick('slickFilter', filteredSlick);
    }

    sliderImage.slick('slickGoTo', slideNumber, true);
    sliderDescr.slick('slickGoTo', slideNumber, true);
  });

  $('.questions__item-info').hide();

  $('.questions__item-top').on('click', function () {
    if ($(this).siblings('.questions__item-info').is(":visible")) {
      $(this).siblings('.questions__item-info').slideUp(200);
      $(this).children('.questions__item-arrow').removeClass('questions__item-arrow--active');
    } else {
      $('.questions__item-info').slideUp(200);
      $('.questions__item-arrow').removeClass('questions__item-arrow--active');
      $(this).siblings('.questions__item-info').slideDown(200);
      $(this).children('.questions__item-arrow').addClass('questions__item-arrow--active');
    }
  });

  $('.advantages__list').hide();
  $('.advantages__img').hide();
  $('.advantages__list--enamel').show();
  $('.advantages__img--enamel').show();
  $('.advantages__material-item--enamel').addClass('active-material');
  $('.advantages__material-item--enamel').on('click', function () {
    $('.advantages__list').hide();
    $('.advantages__img').hide();
    $('.advantages__list--enamel').show();
    $('.advantages__img--enamel').show();
    $('.advantages__material-item').removeClass('active-material');
    $('.advantages__material-item--enamel').addClass('active-material');
  });
  $('.advantages__material-item--laminate').on('click', function () {
    $('.advantages__list').hide();
    $('.advantages__img').hide();
    $('.advantages__list--laminate').show();
    $('.advantages__img--laminate').show();
    $('.advantages__material-item').removeClass('active-material');
    $('.advantages__material-item--laminate').addClass('active-material');
  });
  $('.advantages__material-item--3dlaminate').on('click', function () {
    $('.advantages__list').hide();
    $('.advantages__img').hide();
    $('.advantages__list--3dlaminate').show();
    $('.advantages__img--3dlaminate').show();
    $('.advantages__material-item').removeClass('active-material');
    $('.advantages__material-item--3dlaminate').addClass('active-material');
  });
  $('.advantages__material-item--veneer').on('click', function () {
    $('.advantages__list').hide();
    $('.advantages__img').hide();
    $('.advantages__list--veneer').show();
    $('.advantages__img--veneer').show();
    $('.advantages__material-item').removeClass('active-material');
    $('.advantages__material-item--veneer').addClass('active-material');
  });
  $('.advantages__material-item--leather').on('click', function () {
    $('.advantages__list').hide();
    $('.advantages__img').hide();
    $('.advantages__list--leather').show();
    $('.advantages__img--leather').show();
    $('.advantages__material-item').removeClass('active-material');
    $('.advantages__material-item--leather').addClass('active-material');
  });
  $('.advantages__material-item--glass').on('click', function () {
    $('.advantages__list').hide();
    $('.advantages__img').hide();
    $('.advantages__list--glass').show();
    $('.advantages__img--glass').show();
    $('.advantages__material-item').removeClass('active-material');
    $('.advantages__material-item--glass').addClass('active-material');
  });

  $('.comfort__img').hide();
  $('.comfort__img--solid').show();
  $('.comfort__color--solid').addClass('active-color');
  $('.comfort__color--solid').on('click', function () {
    $('.comfort__img').hide();
    $('.comfort__img--solid').show();
    $('.comfort__color').removeClass('active-color');
    $('.comfort__color--solid').addClass('active-color');
  });
  $('.comfort__color--combined').on('click', function () {
    $('.comfort__img').hide();
    $('.comfort__img--combined').show();
    $('.comfort__color').removeClass('active-color');
    $('.comfort__color--combined').addClass('active-color');
  });
  $('.comfort__color--combinedwith').on('click', function () {
    $('.comfort__img').hide();
    $('.comfort__img--combinedwith').show();
    $('.comfort__color').removeClass('active-color');
    $('.comfort__color--combinedwith').addClass('active-color');
  });

  $('.comfort__wrap--mobile').slick({
    infinite: false,
    fade: true,
    infinite: true,
    cssEase: 'linear',
    dots: true,
    speed: 200
  });

  $('.advantages__slider').slick({
    infinite: false,
    fade: true,
    infinite: true,
    cssEase: 'linear',
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 200,
    asNavFor: '.advantages__material--mobile',
  });

  $('.advantages__material--mobile').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: '.advantages__slider',
    dots: true,
    speed: 200,
    focusOnSelect: true
  });

  //remove active class from all thumbnail slides
  $('.advantages__material--mobile .slick-slide').removeClass('slick-active');

  //set active class to first thumbnail slides
  $('.advantages__material--mobile .slick-slide').eq(0).addClass('slick-active');

  // On before slide change match active thumbnail to current slide
  $('.advantages__slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    var mySlideNumber = nextSlide;
    $('.advantages__material--mobile .slick-slide').removeClass('slick-active');
    $('.advantages__material--mobile .slick-slide').eq(mySlideNumber).addClass('slick-active');
  });

  //UPDATED

  $('.advantages__slider').on('afterChange', function (event, slick, currentSlide) {
    $('.content').hide();
    $('.content[data-id=' + (currentSlide + 1) + ']').show();
  });

  $('.production__wrap--mobile').slick({
    infinite: false,
    fade: true,
    infinite: true,
    cssEase: 'linear',
    dots: true,
  });

  $('.quality__wrap--mobile').slick({
    infinite: false,
    fade: true,
    infinite: true,
    cssEase: 'linear',
    dots: true,
  });


  $('.comfort__item[data-slide]').click(function (e) {
    e.preventDefault();
    var slideno = $(this).data('slide');
    $('.comfort__wrap--mobile').slick('slickGoTo', slideno - 1);
  });

  $('.comfort__color[data-slide]').click(function (e) {
    e.preventDefault();
    var slideno = $(this).data('slide');
    $('.comfort__wrap--mobile').slick('slickGoTo', slideno - 1);
  });

  $('.advantages__wrap--mobile .advantages__list').show();
  $('.advantages__wrap--mobile .advantages__img').show();

  $('.quality__item[data-slide]').click(function (e) {
    e.preventDefault();
    var slideno = $(this).data('slide');
    $('.quality__wrap--mobile').slick('slickGoTo', slideno - 1);
  });

  $('.tel-mask').mask('+7 (000) 000-00-00');

  $(".inputfile").change(function () {
    var f_name = [];
    for (var i = 0; i < $(this).get(0).files.length; ++i) {
      f_name.push(" " + $(this).get(0).files[i].name);
    }
    $("#form-show").val(f_name.join(", "));
  });
  $(".inputfile2").change(function () {
    var f_name = [];
    for (var i = 0; i < $(this).get(0).files.length; ++i) {
      f_name.push(" " + $(this).get(0).files[i].name);
    }
    $("#form-show2").hide(0);
    $("#form-show2").val(f_name.join(", "));
    if ($("#form-show2").val() != "") {
      $("#form-show2").show(0);
    }
  });

  $('.questions__wrap--desctop').slick({
    vertical: true,
    verticalSwiping: true,
    arrows: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    adaptiveHeight: true,
  });

  $('.questions__btn--desctop').on('click', function () {
    event.preventDefault();
    $('.questions__wrap').slick('slickNext');
    $('.questions__item-info').hide(200);
    $('.questions__item-arrow').removeClass('questions__item-arrow--active');
  });

  $('.questions__wrap--tablet .questions__item').hide();
  $('.questions__wrap--tablet .questions__item:nth-child(1)').show();
  $('.questions__wrap--tablet .questions__item:nth-child(2)').show();
  $('.questions__wrap--tablet .questions__item:nth-child(3)').show();
  $('.questions__wrap--tablet .questions__item:nth-child(4)').show();

  $('.questions__btn--tablet').on('click', function () {
    event.preventDefault();
    $('.questions__wrap--tablet .questions__item').show(200);
  });

  $('.header__mobile-btn').on('click', function (event) {
    $('.header__list').toggleClass('header__list--visible');
  });

  $('.mainfilter a').on('click', function (event) {
    if (!$(this).hasClass("active")) {

      history.replaceState({ page: 1 }, "", "?filter=" + $(this).data("filter") + "&option=");

      mixer.destroy();

      currentLimit = 12;
      incrementAmount = 200;
      $(".catalog__actions").show(0);

      $('.mainfilter a').removeClass("active");
      $(this).addClass("active");
      catalog($(this).data("filter"), "", "");
    }
    return false;
  });

  $('.detailfilter button').on('click', function (event) {
    $(this).parents("ul").find("button").removeClass("active");
    $(this).addClass("active");
  });

  $('.header__list a').on('click', function (event) {
    if (!$(this).parent("li").hasClass("has-submenu")) {
      $('.header__list').removeClass('header__list--visible');
    }
  });

  // $('.catalog-page__item').magnificPopup({
  //   type: 'inline',
  //   removalDelay: 500,
  //   mainClass: 'mfp-fade',
  // });

  $('a.js-value').click(function () {
    $(".order__form .target").val($(this).data("target"));
    $('.form-id').val($(this).data("form"));
  });

  $(window).scroll(function () {
    if ($(this).scrollTop() > 400) {
      $('.top-btn').fadeIn();
      $('.header').addClass('header--sect');
    } else {
      $('.top-btn').fadeOut();
      $('.header').removeClass('header--sect');
    }

    if ($("*").is("#contact")) {
      if ((($(this).scrollTop() + $(window).height() - 150) > $("#contact").offset().top) && (!contactVisible)) {
        contactVisible = true;
        var interval = setInterval(function () {
          if ($("body").hasClass("ymConn")) {
            yaCounter54949111.reachGoal('kontakty');
            clearInterval(interval);
          }
        }, 2000);
      }
    }
  });

  $('.top-btn').click(function () {
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
  });

  $('.has-submenu > a').click(function () {
    return false;
  });

  var form = document.getElementsByTagName('form');
  for (var i = 0; i < form.length; i++) {
    form[i].addEventListener('submit', async function (evt) {
      evt.preventDefault();

      var th = $(this);

      try {
        await fetch('/send-email', {
          method: 'POST',
          body: new FormData(this),
        });

        yaCounter54949111.reachGoal(th.find(".target").val());
        yaCounter54949111.reachGoal("forms");

        yaCounter86537628.reachGoal(th.find(".target").val());
        yaCounter86537628.reachGoal("forms");

        ga('send', 'event', 'form', th.find(".target").val());
        ga('send', 'event', 'form', 'forms');
        ga('send', 'event', 'form', 'submit');

        fbq('track', 'Lead');

        // $.magnificPopup.open({
        //   items:
        //   {
        //     src: '#thanks',
        //     type: 'inline'
        //   }
        // });
        setTimeout(function () {
          th.trigger("reset");
          // $.magnificPopup.close();
        }, 5000);
      } catch { }

      if (localStorage.getItem("utm") !== null) {
        var json = JSON.parse(localStorage.getItem("utm")),
          input;
        for (var key in json) {
          input = document.createElement("input");
          input.setAttribute("type", "hidden");
          input.setAttribute("name", key);
          input.setAttribute("value", json[key]);
          f.appendChild(input);
        }
      }
    }, false);
  }



  // $('.order-btn').magnificPopup({
  //   type: 'inline',
  //   removalDelay: 500,
  //   mainClass: 'mfp-fade',
  // });

  $('.comfort__item').hover(
    function () {
      $(this).find(".comfort__point").show(0);
    },
    function () {
      $(this).find(".comfort__point").hide(0);
    });

  $('.quality__item').hover(
    function () {
      $(this).find(".quality__point").show(0);
    },
    function () {
      $(this).find(".quality__point").hide(0);
    });

  heights();
});

$(window).resize(function () {
  setTimeout(function () {
    heights();
  }, 600);
});

function filter() {
  var vars = [], hash;

  if (window.location.href.indexOf('filter') > -1) {
    if (window.location.href.indexOf('?') > -1) {
      var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
      for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
      }
      filter = vars["filter"];
      option = vars["option"];
      //	utmSave();
      catalog(filter, option);
    }
  } else {
    //	utmRemove();
    if (window.location.href.indexOf('catalog') > -1) {
      catalog("cupboard", "", "");
    }
  }
  return false;
}

if (document.referrer.indexOf(document.domain) === -1) {
  if (window.location.search.trim() !== "") {
    utmSave();
  } else {
    utmRemove();
  }
}

function utmSave() {

  var str = window.location.search.replace("?", "");
  var arr = str.trim().split("&"), ar = [], obj = {};

  for (var key in arr) {

    ar = arr[key].trim().split("=");
    if (ar.length === 2) {
      obj[ar[0]] = ar[1];

    }
  }
  localStorage.setItem("utm", JSON.stringify(obj));

}

function utmRemove() {
  if (localStorage.getItem('utm') !== null) {
    localStorage.removeItem('utm');
  }
}

function catalog(filter, option) {

  $('#cube-loader').removeClass('done');
  $('body').addClass('overflow-hidden');

  setTimeout(function ($) {

    $(".catalog__text, .catalog__banner").hide(0);
    $(".catalog__text.text-" + filter + ", .catalog__banner.banner-" + filter).show(0);

    $(".catalog__container").empty();
    $(".catalog__container").load("/catalog/" + filter + mobile + ".html", function () {

      // sliderImage = $(".catalog__container .popup__slider").slick({
      //   fade: true,
      //   infinite: true,
      //   cssEase: 'linear',
      //   speed: 200,
      //   asNavFor: '.popup__desc-slider'
      // });

      // sliderDescr = $(".catalog__container .popup__desc-slider").slick({
      //   asNavFor: '.popup__slider',
      //   arrows: false,
      //   fade: true,
      //   infinite: true,
      //   cssEase: 'linear',
      //   speed: 200
      // });

      $(".catalog__container .popup__slide-link").fancybox({
        helpers: {
          overlay: {
            css: { 'background': 'rgba(11, 11, 11, 0.8)' }
          }
        }
      })

      // $(".catalog__container .collection-name").magnificPopup({
      //   type: 'inline',
      //   removalDelay: 500,
      //   mainClass: 'mfp-fade',
      //   callbacks: {
      //     close: function () {
      //       $("html").css("overflow", "auto");
      //     },
      //     open: function () {
      //       $("html").css("overflow", "hidden");
      //       $(".mfp-wrap").css("overflow", "hidden auto");
      //       $(".popup__slider").slick("setPosition");
      //       $(".popup__desc-slider").slick("setPosition");
      //     },
      //   }
      // })

      // $(".catalog__container .popup__btn").magnificPopup({
      //   type: 'inline',
      //   removalDelay: 500,
      //   mainClass: 'mfp-fade',
      // });

      // $('.order-btn').magnificPopup({
      //   type: 'inline',
      //   removalDelay: 500,
      //   mainClass: 'mfp-fade',
      // });

      mixer = mixitup(".catalog__container .catalog__list", {
        multifilter: {
          enable: true
        },
        pagination: {
          limit: currentLimit
        },
        classNames: {
          elementPageList: 'pagination-links'
        },
        selectors: {
          control: '.detailfilter-' + filter + ' button'
        },
        callbacks: {
          onMixStart: function (state, futureState) {
            newoption = $(".detailfilter .mixitup-control-active").data("filter");
            if (newoption != "") {
              newoption = newoption.split("-")[1];
            }
            history.replaceState({ page: 1 }, "", "?filter=" + filter + "&option=" + newoption);
          },
          onMixEnd: function (state, futureState) {
            handleMixEnd(state);
          }
        }
      });

      $('.mainfilter .' + filter).addClass("active");
      $('.filter-select').text($('.mainfilter .active').text());

      switch (filter) {
        case 'cupboard':
          mixitup(".catalog__container .catalog__list").setFilterGroupSelectors('cupboard-style', option != "" ? ".cupboard-" + option : "");
          mixitup(".catalog__container .catalog__list").setFilterGroupSelectors('cupboard-door', '');
          break;
        case 'wardrobe':
          mixitup(".catalog__container .catalog__list").setFilterGroupSelectors('wardrobe-style', option != "" ? ".wardrobe-" + option : "");
          break;
        case 'other':
          mixitup(".catalog__container .catalog__list").setFilterGroupSelectors('other-type', option != "" ? ".other-" + option : "");
          break;
        default:
          break;
      }
      mixitup(".catalog__container .catalog__list").parseFilterGroups();

      //Сброс второго фильтра (тип открывания дверей) при переключении первого
      var i;
      var els = $(".detailfilter-cupboard .option button");
      for (i = 0; i < els.length; i++) {
        els[i].addEventListener('click', function () {
          mixitup(".catalog__container .catalog__list").setFilterGroupSelectors('cupboard-door', '');
        });
      }
    });

    setTimeout(function () {
      $('#cube-loader').addClass('done');
      $('body').removeClass('overflow-hidden');
    }, 1200);
  }, 300);
}

function handleMixEnd(state) {
  if (state.activePagination.limit >= state.totalMatching) {
    loadMoreEl.disabled = true;
    $(loadMoreEl).addClass("disable");
  } else if (loadMoreEl.disabled) {
    loadMoreEl.disabled = false;
    $(loadMoreEl).removeClass("disable");
  }
}

function repaintMode() {

  if ($(window).width() < 768) {
    // fancybox initialaze & options //
    $("[data-fancybox]").fancybox({
      selector: '.mix:visible a',
      loop: true,
      hash: true,
      transitionEffect: "slide",
      /* zoom VS next////////////////////
      clickContent - i modify the deafult - now when you click on the image you go to the next image - i more like this approach than zoom on desktop (This idea was in the classic/first lightbox) */
      clickContent: function (current, event) {
        return current.type === "image" ? "next" : false;
      }
    });
  }
}

$(document).ready(function () {

  setTimeout(function () {
    var elem = document.createElement('script');
    elem.type = 'text/javascript';
    elem.src = "//api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Abc51f7eaa7cfe8b1f22e3591d05aa4cfd103f07cdd1b5c35353c2ac3a6b6826a&amp;width=100%25&amp;&amp;lang=ru_RU&amp;scroll=false";

    document.getElementById('map').appendChild(elem);

    (function (m, e, t, r, i, k, a) {
      m[i] = m[i] || function () {
        (m[i].a = m[i].a || []).push(arguments)
      };
      m[i].l = 1 * new Date();
      k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
      (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    $("body").addClass("ymConn");

    !function (f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function () {
        n.callMethod ?
          n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
      'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '364548827793450');
    fbq('track', 'PageView');


    (function (w, d, u) {
      var s = d.createElement('script');
      s.async = true;
      s.src = u + '?' + (Date.now() / 60000 | 0);
      var h = d.getElementsByTagName('script')[0];
      h.parentNode.insertBefore(s, h);
    })(window, document, 'https://cdn.bitrix24.ru/b2399491/crm/site_button/loader_3_q6v4tx.js');


    var _tmr = window._tmr || (window._tmr = []);
    _tmr.push({ id: "3136628", type: "pageView", start: (new Date()).getTime(), pid: "USER_ID" });
    (function (d, w, id) {
      if (d.getElementById(id)) return;
      var ts = d.createElement("script");
      ts.type = "text/javascript";
      ts.async = true;
      ts.id = id;
      ts.src = "https://top-fwz1.mail.ru/js/code.js";
      var f = function () {
        var s = d.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(ts, s);
      };
      if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
      } else {
        f();
      }
    })(document, window, "topmailru-code");


  }, 5000);
});
